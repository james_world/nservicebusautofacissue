﻿using System;
using Topshelf;

namespace TryReproNSBRestartBug
{
    static class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(host =>
            {
                host.Service<ServiceController>(service =>
                {
                    service.ConstructUsing(worker => new ServiceController());
                    service.WhenStarted(worker => worker.Start());
                    service.WhenStopped(worker => worker.Stop());
                });

                host.RunAsLocalSystem();

                host.SetDescription("Test");
                host.SetDisplayName("Test");
                host.SetServiceName("Test");
            });

            Console.ReadLine();


        }
    }
}
