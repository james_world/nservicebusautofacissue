﻿using System.Configuration;
using Autofac;
using NServiceBus;
using NServiceBus.Persistence;

namespace TryReproNSBRestartBug
{
    public class ServiceController
    {
        private IContainer _container;
        private IStartableBus _bus;

        public void Start()
        {
            var builder = new ContainerBuilder();
            _container = builder.Build();
            _bus = ConfigureNServiceBus(_container);
        }

        private IStartableBus ConfigureNServiceBus(ILifetimeScope scope)
        {
            var nServiceBusConnectionString = ConfigurationManager.ConnectionStrings["NServiceBusDb"].ConnectionString;

            var configuration = new BusConfiguration();
            configuration.UseContainer<AutofacBuilder>(c => c.ExistingLifetimeScope(scope));
            configuration.UseSerialization<JsonSerializer>();
            configuration.RijndaelEncryptionService();
            configuration.UsePersistence<NHibernatePersistence>().ConnectionString(nServiceBusConnectionString);
            configuration.UseTransport<MsmqTransport>().ConnectionString("cacheSendConnection=true");
            configuration.Transactions().EnableDistributedTransactions();

            configuration.EnableInstallers();

            var bus = Bus.Create(configuration);

            bus.Start();

            return bus;
        }

        public void Stop()
        {
            // Uncomment to fix bug
            // _bus.Dispose();

            _container.Dispose();
        }
    }
}